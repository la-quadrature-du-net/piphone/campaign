Campaign Manager for PiPhone project
====================================

Installation
------------

### Create a python virtual environment and activate it


    virtualenv .env
    source .env/bin/activate


### Install package

For production:

    python setup.py install

For development:

    python setup.py develop




Configuration
-------------

Create **settings.py** from **settings.py.sample** inside the **picampaign** module and edit it:

 * modify the value of **SECRET_KEY**
 * set database configuration according to what type of database you want to use
 * according to your localization, adjust **LANGUAGE_CODE** and **TIME_ZONE**
 * in production environment, set **DEBUG** value to **False**
 * in production environment, set **TEMPLATE_DEBUG** value to **False**
 

### Synchronize database

    ./manage.py migrate


If migrate ran previously, commands are:

    ./manage.py makemigrations
    ./manage.py migrate

To create a superuser, run:

    ./manage.py createsuperuser

### Run website

    ./manage.py runserver


Then open your browser to http://127.0.0.1:8000/admin


Documentation
-------------

The campaign API is self-documented via [drfdocs](https://github.com/manosim/django-rest-framework-docs)
and it is available at the url /docs/ of your instance.
