from django.conf import settings
from django.test import TestCase
from django.contrib.auth.models import User

from picampaign.campaign.models import Campaign, CampaignContact, Argumentary
from picampaign.contact.models import Contact
from picampaign.campaign.admin import CampaignAdmin

class ArgumentaryMethodTests(TestCase):
    def test_str(self):
        campaign = Campaign(
            title='Campaign Title',
            start_date='2000-01-01',
            end_date='2100-12-31'
        )
        argumentary = Argumentary(
            campaign=campaign,
            text='Argument number 1'
        )
        self.assertEqual(str(argumentary), 'Argumentary for Campaign Title')

class CampaignMethodTests(TestCase):
    def test_str(self):
        campaign = Campaign(
            title='Campaign Title',
            start_date='2000-01-01',
            end_date='2100-12-31'
        )
        self.assertEqual(str(campaign), campaign.title)

class CampaignContactMethodTests(TestCase):
    def setUp(self):
        self.campaign = Campaign(
            title='Campaign Title',
            start_date='2000-01-01',
            end_date='2100-12-31'
        )
        self.contact = Contact.objects.create(
            first_name='Victor',
            last_name='Hugo',
            birthdate='1802-02-26',
        )
        self.campaigncontact = CampaignContact(
            campaign=self.campaign,
            contact=self.contact
        )

    def test_all_groups_is_empty(self):
        self.assertEqual(self.campaigncontact.all_groups(),[])

    def test_str(self):
        self.assertEqual(str(self.campaigncontact), 'Contact %(contact)s on %(title)s' % {'contact': self.contact, 'title': self.campaign.title})
