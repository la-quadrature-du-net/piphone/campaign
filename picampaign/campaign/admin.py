from django.contrib import admin
from modeltranslation.admin import TranslationAdmin, TranslationStackedInline

from picampaign.campaign.models import Campaign, CampaignContact, Argumentary
from picampaign.organization.models import Organization
from picampaign.importer.models import Importer
from picampaign.campaign.translation import CampaignTranslationOptions, ArgumentaryTranslationOptions


class InlineContact(admin.TabularInline):
    model = CampaignContact

class InlineImporter(admin.TabularInline):
    model = Importer

class InlineTranslationArgumentary(TranslationStackedInline):
    model = Argumentary

class CampaignAdmin(TranslationAdmin):
    inlines = [InlineTranslationArgumentary, InlineContact, InlineImporter]
    list_display = ('title', 'organization', 'start_date', 'end_date', 'phone_filter',)
    list_filter = ('organization', 'start_date', 'end_date',)
    list_select_related = ('organization',)
    search_fields = ['title', 'organization__name']

    def get_queryset(self, request):
        qs = super(CampaignAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        user_orgs = [x.id for x in request.user.organizations.all()]
        return qs.filter(organization__in=user_orgs)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "organization":
            if request.user.is_superuser:
                orgs = Organization.objects.all()
            else:
                orgs = request.user.organizations.all()
            query = {'id__in': [x.id
                                for x in orgs]}
            kwargs["queryset"] = Organization.objects.filter(**query)
        return super(CampaignAdmin, self).formfield_for_foreignkey(db_field,
                                                                   request,
                                                                   **kwargs)
class CampaignContactAdmin(admin.ModelAdmin):
    list_display = ('contact', 'campaign', 'weight')
    list_select_related = ('contact', 'campaign',)
    list_filter = ('campaign',)
    search_fields = ['campaign__title', 'contact__first_name', 'contact__last_name']

    def get_queryset(self, request):
        qs = super(CampaignContactAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        user_orgs = [x.id for x in request.user.organizations.all()]
        return qs.filter(campaign__organization__in=user_orgs)


class ArgumentaryAdmin(TranslationAdmin):
    list_display = ('title', 'campaign',)
    list_select_related = ('campaign',)
    list_filter = ('campaign',)

    def get_queryset(self, request):
        qs = super(ArgumentaryAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        user_orgs = [x.id for x in request.user.organizations.all()]
        return qs.filter(campaign__organization__in=user_orgs)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "campaign":
            if request.user.is_superuser:
                orgs = Organization.objects.all()
            else:
                orgs = request.user.organizations.all()
            query = {'organization__in': orgs}
            kwargs["queryset"] = Campaign.objects.filter(**query)
        return super(ArgumentaryAdmin, self).formfield_for_foreignkey(db_field,
                                                                      request,
                                                                      **kwargs)
admin.site.register(Campaign, CampaignAdmin)
admin.site.register(CampaignContact, CampaignContactAdmin)
admin.site.register(Argumentary, ArgumentaryAdmin)
