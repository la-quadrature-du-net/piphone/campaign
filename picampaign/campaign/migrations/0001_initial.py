# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0001_initial'),
        ('organization', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Argumentary',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('lang', models.CharField(max_length=5, verbose_name='language', choices=[(b'en', 'English'), (b'de', 'German'), (b'fr', 'French')])),
                ('text', models.TextField(null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Campaign',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('description', models.CharField(max_length=512, blank=True)),
                ('start_date', models.DateField()),
                ('end_date', models.DateField()),
                ('default_lang', models.CharField(max_length=5, verbose_name='language', choices=[(b'en', 'English'), (b'de', 'German'), (b'fr', 'French')])),
                ('organization', models.ForeignKey(related_name='campaigns', to='organization.Organization')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CampaignContact',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('weight', models.IntegerField(default=0)),
                ('campaign', models.ForeignKey(to='campaign.Campaign')),
                ('contact', models.ForeignKey(to='contact.Contact')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='argumentary',
            name='campaign',
            field=models.ForeignKey(to='campaign.Campaign'),
            preserve_default=True,
        ),
    ]
