from picampaign.campaign.models import (Campaign, CampaignContact,
                                        Argumentary)
from picampaign.contact.models import Phone
from picampaign.organization.serializers import GroupSerializer
from rest_framework import serializers


class ArgumentarySerializer(serializers.ModelSerializer):

    class Meta:
        model = Argumentary
        fields = ('text', 'title')

class PhoneSerializer(serializers.ModelSerializer):

    class Meta:
        model = Phone
        fields = ('phone',)

class CampaignSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Campaign
        fields = ('id', 'title', 'description', 'start_date', 'end_date')

class CampaignContactSerializer(serializers.HyperlinkedModelSerializer):

    full_name = serializers.ReadOnlyField(source='contact.full_name')
    first_name = serializers.ReadOnlyField(source='contact.first_name')
    last_name = serializers.ReadOnlyField(source='contact.last_name')
    phone = PhoneSerializer(many=True, source='contact.phones')
    groups = GroupSerializer(many=True, source='contact.groups')
    contact_id = serializers.ReadOnlyField(source='contact.id')
    email = serializers.ReadOnlyField(source='contact.mail')
    twitter = serializers.ReadOnlyField(source='contact.twitter')
    photo = serializers.ReadOnlyField(source='contact.get_photo_url')

    class Meta:
        model = CampaignContact
        fields = ('id', 'weight', 'contact_id',
                  'full_name', 'first_name', 'last_name',
                  'phone', 'groups', 'photo', 'email', 'twitter')

    def to_representation(self, instance):
        """
        We want to validate the phone number.
        """
        # First, let's get the campaign
        data = super(CampaignContactSerializer, self).to_representation(instance)
        campaign = instance.campaign
        # We should have a filter parameter for the campaign
        filter = campaign.phone_filter
        if filter == '' or filter is None:
            # No filter, we return all the numbers
            data['phone'] = [a['phone'] for a in data['phone']]
            return data
        else:
            for phone in data['phone']:
                if phone['phone'].startswith(filter):
                    data['phone'] = phone['phone']
                    return data
