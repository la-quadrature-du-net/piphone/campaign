from collections import OrderedDict

from django.test import TestCase

from picampaign.contact.models import Phone, Contact
from picampaign.campaign.models import Argumentary, Campaign, Organization, CampaignContact
from picampaign.campaign.serializers import ArgumentarySerializer, CampaignContactSerializer

class ArgumentarySerializerTest(TestCase):
    def test_serializers(self):
        organization = Organization.objects.create(
                name='Majestic 12',
                sip_key='majestic-12'
        )
        campaign = Campaign.objects.create(
                title='A campaign',
                start_date='2000-01-01',
                end_date='2100-12-31',
                organization=organization
        )
        Argumentary.objects.create(
                text='A pertinent argument',
                title='A title',
                campaign = campaign
        )
        self.assertEqual(ArgumentarySerializer(Argumentary.objects.all(), many=True).data, [OrderedDict([('text', 'A pertinent argument'), ('title', 'A title')])])

class CampaignContactSerializerTest(TestCase):
    def test_to_representation(self):
        organization = Organization.objects.create(
                name='Majestic 12',
                sip_key='majestic-12'
        )
        campaign = Campaign.objects.create(
                title='A campaign',
                start_date='2000-01-01',
                end_date='2100-12-31',
                organization=organization
        )
        contact = Contact.objects.create(
                first_name='Victor',
                last_name='Hugo',
                birthdate='1887-02-26',
        )
        Phone.objects.create(
                phone='0123456789',
                valid=True,
                contact=contact
        )
        Phone.objects.create(
                phone='9876543210',
                valid=True,
                contact=contact
        )
        campaigncontact = CampaignContact(
                campaign=campaign,
                contact=contact
        )
        serializer = CampaignContactSerializer(campaigncontact)
        self.assertEqual(serializer.to_representation(campaigncontact)['phone'], [phone.phone for phone in contact.phones.all()])
        # Let's add a phone filter
        campaign.phone_filter='98'
        self.assertEqual(serializer.to_representation(campaigncontact)['phone'], contact.phones.last().phone)
