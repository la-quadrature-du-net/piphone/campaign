from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from ckeditor.fields import RichTextField

from picampaign.organization.models import Organization
from picampaign.contact.models import Contact


class Campaign(models.Model):
    """Campaign model, describe what have to be achieved"""
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=255)
    description = RichTextField(null=True)
    organization = models.ForeignKey(Organization, related_name='campaigns')
    start_date = models.DateField()
    end_date = models.DateField()
    phone_filter = models.CharField(max_length=20, null=True, blank=True)

    def __str__(self):
        return self.title

class Argumentary(models.Model):
    """Argumentary for a campaign"""
    id = models.AutoField(primary_key=True)
    campaign = models.ForeignKey(Campaign)
    title = models.CharField(max_length=255, verbose_name=_('title'), null=True, blank=True)
    text = RichTextField(null=True)

    class Meta:
        verbose_name = _("Argument")
        verbose_name_plural = _("Arguments")

    def __str__(self):
        args = {'title': self.campaign.title}
        return _('Argumentary for %(title)s') % args

class CampaignContact(models.Model):
    """List contact related to a campaign with a given weight"""
    id = models.AutoField(primary_key=True)
    campaign = models.ForeignKey(Campaign)
    contact = models.ForeignKey(Contact)
    weight = models.IntegerField(default=0)

    class Meta:
        unique_together = (("campaign", "contact"),)

    def __str__(self):
        args = {'contact': self.contact, 'title': self.campaign.title}
        return _('Contact %(contact)s on %(title)s') % args

    def all_groups(self):
        return [x.name for x in self.contact.groups.all()]
