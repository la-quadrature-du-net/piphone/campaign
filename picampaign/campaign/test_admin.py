from django.contrib.admin.sites import AdminSite
from django.contrib.auth.models import User
from django.test import TestCase

from picampaign.campaign.models import Campaign, Contact, CampaignContact, Argumentary
from picampaign.campaign.admin import CampaignAdmin, CampaignContactAdmin, ArgumentaryAdmin
from picampaign.organization.models import Organization

class MockRequest(object):
    pass

class ArgumentaryAdminTest(TestCase):
    def setUp(self):
        self.organization = Organization.objects.create(
            name='Majestic 12',
            sip_key='majestic-12'
        )
        self.campaign = Campaign.objects.create(
            title='Campaign Title',
            start_date='2000-01-01',
            end_date='2100-12-31',
            organization=self.organization
        )
        self.argumentary = Argumentary.objects.create(
            text='A pertinent argument',
            campaign=self.campaign
        )
        self.super_user = User.objects.create_superuser(
            username='john',
            email='johndoe@example.com',
            password='password',
        )
        self.user = User.objects.create_user(
            username='jane',
            email='janedoe@example.com',
            password='password'
        )
        self.site = AdminSite()

    def test_get_queryset(self):
        ma = ArgumentaryAdmin(Argumentary, self.site)
        request = MockRequest()
        # SuperUser should have it all
        request.user = self.super_user
        self.assertEqual(list(ma.get_queryset(request)), list(Argumentary.objects.all()))
        # User with no orgs should have nothing
        request.user = self.user
        self.assertEqual(list(ma.get_queryset(request)), [])
        # User with orgs should have contacts from associated campaign
        request.user.organizations = Organization.objects.all()
        self.assertEqual(list(ma.get_queryset(request)), list(Argumentary.objects.all()))

    def test_formfield_for_foreignkey(self):
        ma = ArgumentaryAdmin(Argumentary, self.site)
        request = MockRequest()
        # Organization fields should be filtered
        request.user = self.user
        form = ma.get_form(request)()
        self.assertHTMLEqual(str(form['campaign']),
                '<div class="related-widget-wrapper">'
                '<select id="id_campaign" name="campaign" required>'
                '<option value="" selected="selected">---------</option>'
                '</select>'
                '</div>'
                )
        # If we add an org, we shoudl have it
        request.user.organizations = Organization.objects.all()
        form = ma.get_form(request)()
        self.assertHTMLEqual(str(form['campaign']),
                '<div class="related-widget-wrapper">'
                '<select id="id_campaign" name="campaign" required>'
                '<option value="" selected="selected">---------</option>'
                '<option value="%d">%s</option>'
                '</select>'
                '</div>' % (self.campaign.id, str(self.campaign)))

class CampaignContactAdminTest(TestCase):
    def setUp(self):
        self.organization = Organization.objects.create(
            name='Majestic 12',
            sip_key='majestic-12'
        )
        self.campaign = Campaign.objects.create(
            title='Campaign Title',
            start_date='2000-01-01',
            end_date='2100-12-31',
            organization=self.organization
        )
        self.contact = Contact.objects.create(
            first_name='Victor',
            last_name='Hugo',
            birthdate='1802-02-26'
        )
        self.campaigncontact = CampaignContact.objects.create(
            campaign=self.campaign,
            contact=self.contact
        )
        self.super_user = User.objects.create_superuser(
            username='john',
            email='johndoe@example.com',
            password='password',
        )
        self.user = User.objects.create_user(
            username='jane',
            email='janedoe@example.com',
            password='password'
        )
        self.site = AdminSite()

    def test_get_queryset(self):
        ma = CampaignContactAdmin(CampaignContact, self.site)
        request = MockRequest()
        # SuperUser should have it all
        request.user = self.super_user
        self.assertEqual(list(ma.get_queryset(request)), list(CampaignContact.objects.all()))
        # User with no orgs should have nothing
        request.user = self.user
        self.assertEqual(list(ma.get_queryset(request)), [])
        # User with orgs should have contacts from associated campaign
        request.user.organizations = Organization.objects.all()
        self.assertEqual(list(ma.get_queryset(request)), list(CampaignContact.objects.all()))

class CampaignAdminTest(TestCase):
    def setUp(self):
        self.organization = Organization.objects.create(
            name='Majestic 12',
            sip_key='majestic-12'
        )
        self.campaign = Campaign.objects.create(
            title='Campaign Title',
            start_date='2000-01-01',
            end_date='2100-12-31',
            organization = self.organization
        )
        self.super_user = User.objects.create_superuser(
            username='john',
            email='johndoe@example.com',
            password='password',
        )
        self.user = User.objects.create_user(
            username='jane',
            email='janedoe@example.com',
            password='password'
        )
        self.site = AdminSite()

    def test_get_queryset(self):
        ma = CampaignAdmin(Campaign, self.site)
        request = MockRequest()
        # SuperUser should have access to everything
        request.user = self.super_user
        self.assertEqual(list(ma.get_queryset(request)), list(Campaign.objects.all()))
        # NormalUser outside of organization should have access to nothing
        request.user = self.user
        self.assertEqual(list(ma.get_queryset(request)), [])
        # NormalUser which is part of orgs should have access to them
        request.user.organizations = Organization.objects.all()
        self.assertEqual(list(ma.get_queryset(request)), list(Campaign.objects.all()))

    def test_formfield_for_foreignkey(self):
        ma = CampaignAdmin(Campaign, self.site)
        request = MockRequest()
        # Organization fields should be filtered
        request.user = self.user
        form = ma.get_form(request)()
        self.assertHTMLEqual(str(form['organization']),
                '<div class="related-widget-wrapper">'
                '<select id="id_organization" name="organization" required>'
                '<option value="" selected="selected">---------</option>'
                '</select>'
                '</div>'
                )
        # If we add an org, we shoudl have it
        request.user.organizations = Organization.objects.all()
        form = ma.get_form(request)()
        self.assertHTMLEqual(str(form['organization']),
                '<div class="related-widget-wrapper">'
                '<select id="id_organization" name="organization" required>'
                '<option value="" selected="selected">---------</option>'
                '<option value="%d">%s</option>'
                '</select>'
                '</div>' % (self.organization.id, str(self.organization)))
