from rest_framework import viewsets
from rest_framework.response import Response
from picampaign.campaign.models import Campaign, CampaignContact, Argumentary
from picampaign.campaign.serializers import (CampaignSerializer,
                                             CampaignContactSerializer,
                                             ArgumentarySerializer)

class CampaignViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint for campaign view
    """
    queryset = Campaign.objects.all()
    serializer_class = CampaignSerializer

class CampaignContactViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint to view contacts related to a campaign
    """
    queryset = CampaignContact.objects.all()
    serializer_class = CampaignContactSerializer

    def list(self, request, campaign_pk=None):
        contacts = self.queryset.filter(campaign=campaign_pk)
        serializer = self.serializer_class(contacts.all(), many=True)
        return Response(serializer.data)

class ArgumentaryViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint to view contacts related to a campaign
    """
    queryset = Argumentary.objects.all()
    serializer_class = ArgumentarySerializer

    def list(self, request, campaign_pk=None):
        argumentaries = self.queryset.filter(campaign=campaign_pk)
        serializer = self.serializer_class(argumentaries.all(), many=True)
        return Response(serializer.data)
