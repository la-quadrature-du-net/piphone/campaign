from rest_framework.test import APIClient
from django.test import TestCase

from picampaign.campaign.models import Argumentary, Organization, Campaign, CampaignContact
from picampaign.contact.models import Contact, Phone

class ViewSetTest(TestCase):
    def setUp(self):
        self.organization = Organization.objects.create(
            name='Majestic 12',
            sip_key='majestic-12'
        )
        self.campaign = Campaign.objects.create(
            title='Campaign Title',
            start_date='2000-01-01',
            end_date='2100-12-31',
            organization=self.organization
        )
        self.argumentary = Argumentary.objects.create(
            text='A pertinent argument',
            title='A title',
            campaign=self.campaign
        )
        self.contact = Contact.objects.create(
            first_name='Victor',
            last_name='Hugo',
            birthdate='1802-02-26'
        )
        self.campaigncontact = CampaignContact.objects.create(
            campaign=self.campaign,
            contact=self.contact
        )
        self.phone = Phone.objects.create(
            phone='0123456789',
            valid=True,
            contact=self.contact
        )

    def test_campaign_viewset(self):
        client = APIClient()
        response = client.get('/campaigns/', format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'[{"id":1,"title":"Campaign Title","description":null,"start_date":"2000-01-01","end_date":"2100-12-31"}]')

    def test_campaign_contact_viewset(self):
        client = APIClient()
        response = client.get('/campaigns/%(cid)d/contacts/' % {'cid': self.campaign.id}, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'[{"id":1,"weight":0,"contact_id":1,"full_name":"Victor Hugo","first_name":"Victor","last_name":"Hugo","phone":["0123456789"],"groups":[],"photo":"","email":"","twitter":""}]')

    def test_campaign_argumentary_viewset(self):
        client = APIClient()
        response = client.get('/campaigns/%(cid)d/arguments/' % {'cid': self.campaign.id}, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'[{"text":"A pertinent argument","title":"A title"}]')
        response = client.get('/campaigns/%(cid)d/arguments/' % {'cid': self.campaign.id }, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'[{"text":"A pertinent argument","title":"A title"}]')

