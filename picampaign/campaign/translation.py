from modeltranslation.translator import translator, TranslationOptions

from picampaign.campaign.models import Campaign, Argumentary

class CampaignTranslationOptions(TranslationOptions):
    fields = ('title', 'description')

class ArgumentaryTranslationOptions(TranslationOptions):
    fields = ('title', 'text')

translator.register(Campaign, CampaignTranslationOptions)
translator.register(Argumentary, ArgumentaryTranslationOptions)
