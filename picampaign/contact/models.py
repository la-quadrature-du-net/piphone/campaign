from django.db import models
from django.utils.translation import ugettext_lazy as _

class Contact(models.Model):
    """Contact model. Person to be called by users"""
    id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)
    birthdate = models.DateField(blank=True, null=True)
    twitter = models.CharField(max_length=64, blank=True)
    mail = models.CharField(max_length=255, blank=True)
    photo = models.URLField(max_length=500, blank=True)

    def __str__(self):
        return _(u'%(firstname)s %(lastname)s') % {'firstname': self.first_name,
                                                  'lastname': self.last_name}

    def full_name(self):
        return _(u'%(firstname)s %(lastname)s') % {'firstname': self.first_name,
                                                  'lastname': self.last_name}
    full_name.admin_order_field = 'first_name'

    def set_valid_phone(self, valid_phone):
        """
        We want to set a phone as valid - so we must invalidate all the other ones
        """
        if isinstance(valid_phone, Phone):
            Phone.objects.filter(contact=self).update(valid=False)
            Phone.objects.filter(id=valid_phone.id).update(valid=True)

    def get_photo_url(self):
        if self.photo:
            return self.photo
        return ''

class Phone(models.Model):
    """Phone model, sed to keep track of which phone is working."""
    id = models.AutoField(primary_key=True)
    phone = models.CharField(max_length=32)
    contact = models.ForeignKey(Contact, related_name='phones')
    valid = models.BooleanField(default=False)

    def __str__(self):
        return _(u'%(phone)s') % {'phone': self.phone}
