from django.contrib import admin

from picampaign.contact.models import Contact, Phone
from picampaign.organization.models import Group


class ContactFilterByGroup(admin.SimpleListFilter):
    title = 'groups'
    parameter_name = 'groups'

    def lookups(self, request, model_admin):
        if request.user.is_superuser:
            contacts = Contact.objects.all().values_list('groups__id', 'groups__name')
        else:
            user_orgs = [ x.id for x in request.user.organizations.all() ]
            contacts = Contact.objects.filter(group__type__organization__in=user_orgs).values_list('groups__id', 'groups__name')

        return ((g[0], g[1],) for g in sorted(set(contacts), key=lambda g: g[1]))

    def queryset(self, request, queryset):
        # self.value has the groyp id
        return queryset.filter(groups__id=self.value())


class InlinePhone(admin.TabularInline):
    model = Phone


class ContactAdmin(admin.ModelAdmin):
    list_filter = (ContactFilterByGroup,)
    inlines = [InlinePhone]
    list_display = ['full_name', 'mail', 'twitter']
    search_fields = ('first_name', 'last_name')


class PhoneAdmin(admin.ModelAdmin):
    list_display = ['phone', 'valid', 'contact']
    search_fields = ('phone',)


admin.site.register(Phone, PhoneAdmin)
admin.site.register(Contact, ContactAdmin)
