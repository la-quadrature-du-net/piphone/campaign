from unittest import mock

from django.test import TestCase
from django.core.files import File

from picampaign.contact.models import Contact, Phone

class ContactMethodsTest(TestCase):
    def test_full_name(self):
        contact = Contact(first_name='Victor', last_name='Hugo')
        self.assertEqual(contact.full_name(), 'Victor Hugo')

    def test_set_valid_phone(self):
        contact = Contact.objects.create(
                first_name='Victor',
                last_name='Hugo'
        )
        phone1 = Phone.objects.create(
                phone='0123456789',
                contact=contact,
                valid = True
        )
        phone2 = Phone.objects.create(
                phone='0123456789',
                contact=contact,
                valid=False
        )
        # Let's check we're in the correct state first
        self.assertEqual(phone1.valid, True)
        self.assertEqual(phone2.valid, False)
        # Let's change the validity
        contact.set_valid_phone(phone2)
        phone1.refresh_from_db()
        phone2.refresh_from_db()
        self.assertEqual(phone1.valid, False)
        self.assertEqual(phone2.valid, True)

    def test_get_photo_url(self):
        contact = Contact.objects.create(
                first_name='Victor',
                last_name='Ugo',
        )
        self.assertEqual(contact.get_photo_url(), '')
        contact.photo = 'http://example.com/photo1.jpg'
        self.assertEqual(contact.get_photo_url(), 'http://example.com/photo1.jpg')

class PhoneTest(TestCase):
    def test_str(self):
        contact = Contact.objects.create(
                first_name='Victor',
                last_name='Hugo'
        )
        phone = Phone.objects.create(
                phone='0123456789',
                contact=contact,
                valid=False
        )
        self.assertEqual(str(phone), phone.phone)
