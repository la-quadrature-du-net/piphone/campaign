# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('first_name', models.CharField(max_length=64)),
                ('last_name', models.CharField(max_length=64)),
                ('phone', models.CharField(max_length=32)),
                ('twitter', models.CharField(max_length=64, blank=True)),
                ('mail', models.CharField(max_length=255, blank=True)),
                ('photo', models.ImageField(upload_to=b'contacts/photos', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
