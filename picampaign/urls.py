from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from rest_framework_nested import routers

from picampaign.campaign.views import (CampaignViewSet, CampaignContactViewSet,
                                       ArgumentaryViewSet)
from picampaign.feedback.views import FeedbackViewSet, FeedbackExportCSVView
from picampaign.organization.views import (CategoryViewSet, GroupTypeViewSet,
                                            GroupViewSet, OrganizationViewSet)

router = routers.SimpleRouter()
router.register(r'campaigns', CampaignViewSet)

campaign_router = routers.NestedSimpleRouter(router, r'campaigns',
                                             lookup='campaign')
campaign_router.register(r'contacts', CampaignContactViewSet)
campaign_router.register(r'arguments', ArgumentaryViewSet)
campaign_router.register(r'categories', CategoryViewSet)
campaign_router.register(r'feedbacks', FeedbackViewSet)
campaign_router.register(r'grouptypes', GroupTypeViewSet)
campaign_router.register(r'groups', GroupViewSet)
campaign_router.register(r'organization', OrganizationViewSet)


urlpatterns = [
       url(r'^admin/', include(admin.site.urls)),
       url(r'^', include(router.urls)),
       url(r'^', include(campaign_router.urls)),
       url(r'^i18n/', include('django.conf.urls.i18n')),
       url(r'^docs/', include('rest_framework_docs.urls')),
       url(r'^export/', FeedbackExportCSVView),
    ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
