from rest_framework import viewsets
from rest_framework.response import Response

from picampaign.organization.models import (FeedbackCategory, Organization,
                                            GroupType, Group)
from picampaign.organization.serializers import (CategorySerializer,
                                                GroupTypeSerializer,
                                                GroupSerializer,
                                                OrganizationSerializer)


class CategoryViewSet(viewsets.ViewSet):

    queryset = FeedbackCategory.objects.all()
    serializer_class = CategorySerializer

    def list(self, request, campaign_pk):
        orga = Organization.objects.filter(campaigns__id=campaign_pk)[0]
        categories = self.queryset.filter(organization_id=orga.id)
        serializer = self.serializer_class(categories, many=True)
        return Response(serializer.data)

class GroupTypeViewSet(viewsets.ViewSet):

    queryset = GroupType.objects.all()
    serializer_class = GroupTypeSerializer

    def list(self, request, campaign_pk):
        orga = Organization.objects.filter(campaigns__id=campaign_pk)[0]
        grouptype = self.queryset.filter(organization_id=orga.id)
        serializer = self.serializer_class(grouptype, many=True)
        return Response(serializer.data)

class GroupViewSet(viewsets.ViewSet):

    queryset = Group.objects.all()
    serializer_class = GroupSerializer

    def list(self, request, campaign_pk):
        orga = Organization.objects.filter(campaigns__id=campaign_pk)[0]
        groups = self.queryset.filter(type__organization_id=orga.id)
        serializer = self.serializer_class(groups, many=True)
        return Response(serializer.data)

class OrganizationViewSet(viewsets.ViewSet):

    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer

    def list(self, request, campaign_pk):
        orga = Organization.objects.filter(campaigns__id=campaign_pk)[0]
        serializer = self.serializer_class(orga)
        return Response(serializer.data)

