from django.contrib.admin.sites import AdminSite
from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework.test import APIClient

from picampaign.organization.models import Organization, GroupType, Group, FeedbackCategory
from picampaign.organization.admin import OrganizationAdmin, GroupTypeAdmin, GroupAdmin, CategoryAdmin
from picampaign.campaign.models import Campaign

# Create your tests here.
class OrganizationTest(TestCase):
    def test_str(self):
        organization = Organization.objects.create(
                name='Majestic 12',
                sip_key='majestic-12'
        )
        self.assertEqual(str(organization), organization.name)

class GroupTypeTest(TestCase):
    def test_str(self):
        organization = Organization.objects.create(
                name='Majestic 12',
                sip_key='majestic-12'
        )
        grouptype = GroupType.objects.create(
                name='comittee',
                organization=organization
        )
        self.assertEqual(str(grouptype), grouptype.name)

class GroupTest(TestCase):
    def test_str(self):
        organization = Organization.objects.create(
                name='Majestic 12',
                sip_key='majestic-12'
        )
        grouptype = GroupType.objects.create(
                name='comittee',
                organization=organization
        )
        group = Group.objects.create(
            name='group',
            type=grouptype
        )
        self.assertEqual(str(group), group.name)

class FeedbackCategoryTest(TestCase):
    def test_str(self):
        organization = Organization.objects.create(
                name='Majestic 12',
                sip_key='majestic-12'
        )
        feedbackcategory =FeedbackCategory.objects.create(
                name='Feedback',
                organization=organization
        )
        self.assertEqual(str(feedbackcategory), feedbackcategory.name)

class OrganizationViewSet(TestCase):
    def setUp(self):
        self.organization = Organization.objects.create(
                name='Majestic 12',
                sip_key='majestic-12'
        )
        self.campaign = Campaign.objects.create(
                title='Test Campaign',
                start_date='2000-01-01',
                end_date='2100-12-31',
                organization=self.organization
        )

    def test_organization_viewset(self):
        client = APIClient()
        response = client.get('/campaigns/%(cid)s/organization/' % {'cid': self.campaign.id}, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'{"id":1,"name":"Majestic 12","description":"","website":"","logo":null}')

class CategoryViewSet(TestCase):
    def setUp(self):
        self.organization = Organization.objects.create(
                name='Majestic 12',
                sip_key='majestic-12'
        )
        self.campaign = Campaign.objects.create(
                title='Test Campaign',
                start_date='2000-01-01',
                end_date='2100-12-31',
                organization=self.organization
        )
        self.feedbackcategory = FeedbackCategory.objects.create(
                name='Feedback',
                organization=self.organization
        )

    def test_feedbackcategory_viewset(self):
        client = APIClient()
        response = client.get('/campaigns/%(cid)s/categories/' % {'cid': self.campaign.id}, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'[{"id":1,"name":"Feedback"}]')

class GroupViewSetTest(TestCase):
    def setUp(self):
        self.organization = Organization.objects.create(
                name='Majestic 12',
                sip_key='majestic-12'
        )
        self.campaign = Campaign.objects.create(
                title='Test Campaign',
                start_date='2000-01-01',
                end_date='2100-12-31',
                organization=self.organization
        )
        self.grouptype = GroupType.objects.create(
                name='comittee',
                organization = self.organization
        )
        self.group = Group.objects.create(
                name="INTA",
                type=self.grouptype
        )

    def test_grouptype_viewset(self):
        client = APIClient()
        response = client.get('/campaigns/%(cid)s/groups/' % {'cid': self.campaign.id}, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'[{"id":1,"type":"comittee","name":"INTA","media":""}]')

class GroupTypeViewSetTest(TestCase):
    def setUp(self):
        self.organization = Organization.objects.create(
                name='Majestic 12',
                sip_key='majestic-12'
        )
        self.campaign = Campaign.objects.create(
                title='Test Campaign',
                start_date='2000-01-01',
                end_date='2100-12-31',
                organization=self.organization
        )
        self.grouptype = GroupType.objects.create(
                name='comittee',
                organization = self.organization
        )

    def test_grouptype_viewset(self):
        client = APIClient()
        response = client.get('/campaigns/%(cid)s/grouptypes/' % {'cid': self.campaign.id}, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'[{"id":1,"name":"comittee"}]')

class MockRequest(object):
    pass

class OrganizationAdminTest(TestCase):
    def setUp(self):
        self.organization = Organization.objects.create(
            name='Majestic 12',
            sip_key='majestic-12'
        )
        self.super_user = User.objects.create_superuser(
            username='john',
            email='johndoe@example.com',
            password='password',
        )
        self.user = User.objects.create_user(
            username='jane',
            email='janedoe@example.com',
            password='password'
        )
        self.site = AdminSite()

    def test_get_queryset(self):
        ma = OrganizationAdmin(Organization, self.site)
        request = MockRequest()
        request.user = self.super_user
        self.assertEqual(list(ma.get_queryset(request)), list(Organization.objects.all()))
        request.user = self.user
        self.assertEqual(list(ma.get_queryset(request)), [])
        request.user.organizations = Organization.objects.all()
        self.assertEqual(list(ma.get_queryset(request)), list(Organization.objects.all()))

class GroupTypeAdminTest(TestCase):
    def setUp(self):
        self.organization = Organization.objects.create(
            name='Majestic 12',
            sip_key='majestic-12'
        )
        self.grouptype = GroupType.objects.create(
                name='Group',
                organization=self.organization
        )
        self.super_user = User.objects.create_superuser(
            username='john',
            email='johndoe@example.com',
            password='password',
        )
        self.user = User.objects.create_user(
            username='jane',
            email='janedoe@example.com',
            password='password'
        )
        self.site = AdminSite()

    def test_get_queryset(self):
        ma = GroupTypeAdmin(GroupType, self.site)
        request = MockRequest()
        request.user = self.super_user
        self.assertEqual(list(ma.get_queryset(request)), list(GroupType.objects.all()))
        request.user = self.user
        self.assertEqual(list(ma.get_queryset(request)), [])
        request.user.organizations = Organization.objects.all()
        self.assertEqual(list(ma.get_queryset(request)), list(GroupType.objects.all()))

    def test_formfield_forforeignkey(self):
        ma = GroupTypeAdmin(GroupType, self.site)
        request = MockRequest()
        request.user = self.user
        form = ma.get_form(request)()
        self.assertHTMLEqual(str(form['organization']),
                '<div class="related-widget-wrapper">'
                '<select id="id_organization" name="organization" required>'
                '<option value="" selected="selected">---------</option>'
                '</select>'
                '</div>')
        request.user.organizations = Organization.objects.all()
        form = ma.get_form(request)()
        self.assertHTMLEqual(str(form['organization']),
                '<div class="related-widget-wrapper">'
                '<select id="id_organization" name="organization" required>'
                '<option value="" selected="selected">---------</option>'
                '<option value="%(id)d">%(org)s</option>'
                '</select>'
                '</div>' % {'id': self.organization.id, 'org': self.organization})

class GroupAdminTest(TestCase):
    def setUp(self):
        self.organization = Organization.objects.create(
            name='Majestic 12',
            sip_key='majestic-12'
        )
        self.grouptype = GroupType.objects.create(
            name='Group',
            organization=self.organization
        )
        self.group = Group.objects.create(
            name='Test Group',
            type=self.grouptype
        )
        self.super_user = User.objects.create_superuser(
            username='john',
            email='johndoe@example.com',
            password='password',
        )
        self.user = User.objects.create_user(
            username='jane',
            email='janedoe@example.com',
            password='password'
        )

        self.site = AdminSite()

    def test_get_queryset(self):
        ma = GroupAdmin(Group, self.site)
        request = MockRequest()
        request.user = self.super_user
        self.assertEqual(list(ma.get_queryset(request)), list(Group.objects.all()))
        request.user = self.user
        self.assertEqual(list(ma.get_queryset(request)), [])
        request.user.organizations = Organization.objects.all()
        self.assertEqual(list(ma.get_queryset(request)), list(Group.objects.all()))

    def test_formfield_forforeignkey(self):
        ma = GroupAdmin(Group, self.site)
        request = MockRequest()
        request.user = self.user
        form = ma.get_form(request)()
        self.assertHTMLEqual(str(form['type']),
                '<div class="related-widget-wrapper">'
                '<select id="id_type" name="type">'
                '<option value="" selected="selected">---------</option>'
                '</select>'
                '</div>')
        request.user.organizations = Organization.objects.all()
        form = ma.get_form(request)()
        self.assertHTMLEqual(str(form['type']),
                '<div class="related-widget-wrapper">'
                '<select id="id_type" name="type">'
                '<option value="" selected="selected">---------</option>'
                '<option value="%(id)d">%(typ)s</option>'
                '</select>'
                '</div>' % {'id': self.grouptype.id, 'typ': self.grouptype})


class CategoryAdminTest(TestCase):
    def setUp(self):
        self.organization = Organization.objects.create(
            name='Majestic 12',
            sip_key='majestic-12'
        )
        self.group = FeedbackCategory.objects.create(
            name='Test Feedback',
            organization=self.organization
        )
        self.super_user = User.objects.create_superuser(
            username='john',
            email='johndoe@example.com',
            password='password',
        )
        self.user = User.objects.create_user(
            username='jane',
            email='janedoe@example.com',
            password='password'
        )

        self.site = AdminSite()

    def test_get_queryset(self):
        ma = CategoryAdmin(FeedbackCategory, self.site)
        request = MockRequest()
        request.user = self.super_user
        self.assertEqual(list(ma.get_queryset(request)), list(FeedbackCategory.objects.all()))
        request.user = self.user
        self.assertEqual(list(ma.get_queryset(request)), [])
        request.user.organizations = Organization.objects.all()
        self.assertEqual(list(ma.get_queryset(request)), list(FeedbackCategory.objects.all()))

    def test_formfield_forforeignkey(self):
        ma = CategoryAdmin(FeedbackCategory, self.site)
        request = MockRequest()
        request.user = self.user
        form = ma.get_form(request)()
        self.assertHTMLEqual(str(form['organization']),
                '<div class="related-widget-wrapper">'
                '<select id="id_organization" name="organization" required>'
                '<option value="" selected="selected">---------</option>'
                '</select>'
                '</div>')
        request.user.organizations = Organization.objects.all()
        form = ma.get_form(request)()
        self.assertHTMLEqual(str(form['organization']),
                '<div class="related-widget-wrapper">'
                '<select id="id_organization" name="organization" required>'
                '<option value="" selected="selected">---------</option>'
                '<option value="%(id)d">%(org)s</option>'
                '</select>'
                '</div>' % {'id': self.organization.id, 'org': self.organization})

