from modeltranslation.translator import translator, TranslationOptions


from picampaign.organization.models import Organization

class OrganizationTranslationOptions(TranslationOptions):
    fields = ('description',)

translator.register(Organization, OrganizationTranslationOptions)
