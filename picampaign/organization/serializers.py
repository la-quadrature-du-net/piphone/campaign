from django.contrib.sites.shortcuts import get_current_site
from rest_framework import serializers

from picampaign.organization.models import FeedbackCategory, Group, GroupType, Organization


class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = FeedbackCategory
        fields = ('id', 'name')


class GroupSerializer(serializers.ModelSerializer):
    type = serializers.ReadOnlyField(source='type.name')

    class Meta:
        model = Group
        fields = ('id', 'type', 'name', 'media')


class GroupTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = GroupType
        fields = ('id', 'name')


class OrganizationSerializer(serializers.ModelSerializer):
    logo = serializers.SerializerMethodField()
    class Meta:
        model = Organization
        fields = ('id', 'name', 'description', 'website', 'logo',)

    def get_logo(self, obj):
        if obj.logo:
            return obj.logo.url
        return None
