from django.contrib import admin
from picampaign.organization.models import (Organization, Group, GroupType,
                                            FeedbackCategory)


class OrganizationAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        qs = super(OrganizationAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        user_orgs = [x.id for x in request.user.organizations.all()]
        return qs.filter(id__in=user_orgs)


class GroupInline(admin.TabularInline):
    model = Group


class GroupTypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'organization',)
    list_filter = ('organization',)
    inlines = [GroupInline]

    def get_queryset(self, request):
        qs = super(GroupTypeAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        user_orgs = [x.id for x in request.user.organizations.all()]
        return qs.filter(organization__in=user_orgs)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "organization":
            orgs = request.user.organizations.all()
            query = {'id__in': orgs}
            kwargs["queryset"] = Organization.objects.filter(**query)
        return super(GroupTypeAdmin, self).formfield_for_foreignkey(db_field,
                                                                    request,
                                                                    **kwargs)


class GroupAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', 'organization',)
    list_filter = ('type',)
    search_fields = ['name',]

    def get_queryset(self, request):
        qs = super(GroupAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        user_orgs = [x.id for x in request.user.organizations.all()]
        return qs.filter(type__organization__in=user_orgs)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "type":
            orgs = request.user.organizations.all()
            query = {'organization__in': orgs}
            kwargs["queryset"] = GroupType.objects.filter(**query)
        return super(GroupAdmin, self).formfield_for_foreignkey(db_field,
                                                                request,
                                                                **kwargs)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'organization',)
    list_filter = ('organization',)

    def get_queryset(self, request):
        qs = super(CategoryAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        user_orgs = [x.id for x in request.user.organizations.all()]
        return qs.filter(organization__in=user_orgs)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "organization":
            orgs = request.user.organizations.all()
            query = {'id__in': orgs}
            kwargs["queryset"] = Organization.objects.filter(**query)
        return super(CategoryAdmin, self).formfield_for_foreignkey(db_field,
                                                                   request,
                                                                   **kwargs)


admin.site.register(Organization, OrganizationAdmin)
admin.site.register(GroupType, GroupTypeAdmin)
admin.site.register(Group, GroupAdmin)
admin.site.register(FeedbackCategory, CategoryAdmin)
