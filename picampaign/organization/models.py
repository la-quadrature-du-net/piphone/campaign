from django.db import models
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField

from picampaign.contact.models import Contact


class Organization(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=64)
    sip_key = models.CharField(max_length=255)
    users = models.ManyToManyField(User, blank=True,
                                   related_name='organizations')
    description = RichTextField(blank=True)
    logo = models.ImageField(upload_to='organization/logos/', blank=True)
    website = models.URLField(blank=True)

    def __str__(self):
        return self.name


class GroupType(models.Model):
    id = models.AutoField(primary_key=True)
    organization = models.ForeignKey(Organization)
    name = models.CharField(max_length=64)

    def __str__(self):
        return self.name


class Group(models.Model):
    """Group model to qualify contacts"""
    id = models.AutoField(primary_key=True)
    #organization = models.ForeignKey(Organization)
    name = models.CharField(max_length=64)
    type = models.ForeignKey(GroupType, blank=True, null=True)
    media = models.CharField(max_length=255, blank=True)
    contacts = models.ManyToManyField(Contact, blank=True,
                                      related_name='groups')

    def organization(self):
        return self.type.organization

    def __str__(self):
        return self.name


class FeedbackCategory(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=64)
    organization = models.ForeignKey(Organization)

    def __str__(self):
        return self.name
