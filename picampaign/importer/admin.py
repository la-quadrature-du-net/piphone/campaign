from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from picampaign.importer.models import Importer

class ImporterAdmin(admin.ModelAdmin):
    list_display = ['name', 'kind', 'category']
    actions = ['import_action']

    def get_queryset(self, request):
        qs = super(ImporterAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        user_orgs = [x.id for x in request.user.organizations.all()]
        return qs.filter(campaign__organization__in=user_orgs)

    def import_action(self, request, queryset):
        """
        We're going to launch the import of data
        """
        for obj in queryset:
            obj.handle()
            self.message_user(request, _("Imported %(last_imported)s of %(last_count)s contacts") % {
                'last_imported': obj.last_imported,
                'last_count': obj.last_count}, fail_silently='True')
    import_action.short_description = _("Run the selected importers")

admin.site.register(Importer, ImporterAdmin)
