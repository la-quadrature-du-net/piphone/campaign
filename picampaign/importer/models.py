from csv import DictReader
import json


from django.db import models
from django.utils.translation import ugettext_lazy as _


from picampaign.contact.models import Contact
from picampaign.campaign.models import Campaign, CampaignContact
from picampaign.organization.models import Group, GroupType


class Importer(models.Model):
    """Importer model. Used to populate campaign with contacts"""
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=64)
    category = models.CharField(max_length=6, choices=(('update', _('CSV Updater')),
                                                       ('repr', _("Representative format"))))
    file = models.FileField(upload_to='imports/', blank=True, null=True)
    url = models.URLField(blank=True, null=True)
    campaign = models.ForeignKey(Campaign, on_delete=models.SET_NULL, null=True)
    last_count = models.IntegerField(null=True, blank=True)
    last_imported = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return _(u'%(name)s: %(format)s %(type)s importer') % {'format': self.category,
                                                'type': self.kind(),
                                                'name': self.name}

    def kind(self):
        if self.url is None and self.file.name is None:
            return None
        if self.file.name is not None:
            return 'file'
        else:
            return 'url'

    def representative(self, import_data):
        """
        This handle all the representatives data import. The imported_data have all the contacts in
        the representative format (and in a list).
        """
        # We now have all the data ready to be imported.
        # We can count the data available
        self.last_count = len(import_data)

        inserted = 0
        for import_contact in import_data:
            # We want to know if all mandates exists
            if 'birth_date' not in import_contact or 'first_name' not in import_contact or 'last_name' not in import_contact:
                # We do not have enough field to discriminate our contacts
                continue
            updated_contact = {}
            groups = []
            for mandate in import_contact['mandates']:
                # Parltrack gives us a date starting with 9999 if ends has not happens
                # FranceData gives us an empty one
                end_date = mandate.get('end_date', None)
                if end_date is None:
                    continue
                else:
                    if not (end_date == '' or end_date.startswith('9999')):
                        # This mandate has ended
                        continue
                # We want to only use the groups own by the organisation
                for groupType in GroupType.objects.filter(organization=self.campaign.organization,
                        name=mandate['group']['kind']):
                    if 'abbreviation' in mandate['group']:
                        group, added = Group.objects.get_or_create(name=mandate['group']['abbreviation'],
                                type=groupType)
                    else:
                        group, added=Group.objects.get_or_create(name=mandate['group']['name'],
                                type=groupType)
                    groups.append(group)
            if groups == []:
                # The contact have no groups active, he is not to be bothered.
                continue

            # All groups are done
            # If we have a contact item, we can go through it. Otherwise we probably have
            # phone, twitter, etc fields.
            phones = []
            # Let's get the phones
            if import_contact['contacts']['phones'] != []:
                for phone in import_contact['contacts']['phones']:
                    if phone['kind'] == "office phone":
                        phones.append(phone['number'])

            # These are the email adresses used
            if import_contact['contacts']['emails'] != []:
                updated_contact['mail'] = import_contact['contacts']['emails'][0]['email']

            # We're trying to find the twitter accounts
            if import_contact['contacts']['websites'] != []:
                for website in import_contact['contacts']['websites']:
                    if website['kind'] == 'twitter':
                        updated_contact['twitter'] = ''.join(['@', website['url'].split('/')[-1]])
                        break

            # Let's retrieve the photo URL
            if 'photo' in import_contact:
                updated_contact['photo'] = import_contact['photo']

            # Let's get the twitter profile
            if 'twitter' in import_contact:
                updated_contact['twitter'] = import_contact['twitter']
            # Let's update_or_create the contact
            contact, updated = Contact.objects.update_or_create(first_name=import_contact['first_name'],
                                                                last_name=import_contact['last_name'],
                                                                birthdate=import_contact['birth_date'],
                                                                defaults=updated_contact)

            # We want to create the phones
            for phone in phones:
                contact.phones.get_or_create(phone=phone)

            # Let's link the MEP to the group
            contact.groups = groups
            contact.save()
            # We want to link the contact to our current campaign
            campaigncontact, created = CampaignContact.objects.update_or_create(campaign=self.campaign, contact=contact)
            inserted += 1

        # Updating the last imported item
        self.last_imported = inserted
        self.save()

    def handle(self):
        """
        This function is used to import the data described by the Importer.

        We need to parse the provided file, using an appropriate deserilizer/parser,
        and then to create or update contacts.

        The operation should end providing the number of items imported and the number of items submitted.

        Importing mandates from CSV requires to have header with "mandate:kind" in it, each one
        holding a list of groups (mandates) of a specific kind, column separated

        For instance, the comittee of a MEP would be listed in a column named 'group:comittee', separated
        by columns.
        """
        import_data = []
        # First let's check what kind of importer are we running.
        # If it's a file, let's parse it according to our format.
        if self.kind() == 'file':
            with open(self.file.path, u'rU') as f:
                if self.category == 'update':
                    # If we're a csv format, let's use DictReader on a file object
                    reader = DictReader(f)
                    for item in reader:
                        # FIXME: We need to create the groups according to the header
                        # Which means for each mandate:kind columsn we must add a item['mandates']
                        # entry with an end-date of ''
                        # One can belong to different groups
                        mandates = []
                        parsed_item = {}
                        for key in [k for k in item if k.startswith('mandates:')]:
                            for group in item[key].split(','):
                                mandates.append({'group':
                                        {'kind': key.split(':')[1],
                                        'name': group},
                                    'end_date': ''})
                        # Same for phones and websites
                        contacts = {}
                        contacts['phones'] = []
                        for key in [k for k in item if k.startswith('phones:')]:
                            contacts['phones'].append({'kind': 'office phone',
                                'number': item[key]})
                        contacts['emails'] = []
                        for key in [k for k in item if k.startswith('emails:')]:
                            contacts['emails'].append({'kind': key.split(':')[1],
                                'email': item[key]})
                        contacts['websites'] = []
                        for key in [k for k in item if k == 'twitter']:
                            contacts['websites'].append({'url': item[key],
                                'kind': 'twitter'})

                        parsed_item['first_name'] = item['first_name']
                        parsed_item['last_name'] = item['last_name']
                        parsed_item['birth_date'] = item['birth_date']
                        parsed_item['mandates'] = mandates
                        parsed_item['contacts'] = contacts
                        parsed_item['photo'] = item['photo']
                        import_data.append(parsed_item)
                else:
                    # We're in json, let's load from a file
                    import_data = json.load(f)
        # Now, let's get the correct call
        self.representative(import_data)
        return
