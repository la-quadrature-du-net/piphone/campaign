from unittest import mock

from django.contrib.admin.sites import AdminSite
from django.contrib.admin.options import ModelAdmin
from django.contrib.auth.models import User
from django.contrib.messages.storage.fallback import FallbackStorage
from django.test import TestCase, RequestFactory
from django.core.files.uploadedfile import SimpleUploadedFile

from picampaign.importer.models import Importer
from picampaign.contact.models import Contact
from picampaign.campaign.models import Campaign, CampaignContact
from picampaign.organization.models import Group, GroupType, Organization
from picampaign.importer.admin import ImporterAdmin

class ImporterAdminTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
            username='john',
            email='johndoe@example.com',
            password='top_sikret'
        )
        self.super_user = User.objects.create_superuser(
            username='Jane',
            email='janedoe@exmaple.com',
            password='password'
        )
        self.organization = Organization.objects.create(
            name='Majestic 12',
            sip_key='majestic-12'
        )
        self.campaign = Campaign.objects.create(
            title='Test Campaign',
            start_date='2000-01-01',
            end_date='2100-12-31',
            organization=self.organization
        )
        GroupType.objects.create(
            organization=self.organization,
            name='group'
        )
        GroupType.objects.create(
            organization=self.organization,
            name='chamber'
        )
        GroupType.objects.create(
            organization=self.organization,
            name='committee'
        )
        json_file = None
        with open('picampaign/importer/data_tests.json', u'rb') as f:
            json_file = SimpleUploadedFile('data_tests.json', f.read())

        self.importer = Importer.objects.create(
            name='test importer',
            category='repr',
            campaign = self.campaign,
            file=json_file,
            url=''
        )
        self.site = AdminSite()

    def test_get_queryset(self):
        ma = ImporterAdmin(Importer, self.site)
        request = self.factory.get('/admin/picampaign/importers/')
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messags', messages)
        # Superuser can see anything
        request.user = self.super_user
        self.assertEqual(list(ma.get_queryset(request)), list(Importer.objects.all()))
        # Normal user outside of orgs sees nothing
        request.user = self.user
        self.assertEqual(list(ma.get_queryset(request)), [])
        # User in orgs see what they own
        request.user.organizations = Organization.objects.all()
        self.assertEqual(list(ma.get_queryset(request)), list(Importer.objects.all()))

    def test_import_action(self):
        ma = ImporterAdmin(Importer, self.site)
        request = self.factory.get('/admin/picampaign/importers/')
        request.user = self.user
        request.user.organizations = Organization.objects.all()
        # Launch the action now
        self.assertEqual(self.importer.last_imported, None)
        self.assertEqual(self.importer.last_count, None)
        ma.import_action(request, ma.get_queryset(request))
        self.importer.refresh_from_db()
        self.assertEqual(self.importer.last_imported, 3)
        self.assertEqual(self.importer.last_count, 10)

class ImporterTest(TestCase):
    def setUp(self):
        self.organization = Organization.objects.create(
            name='Majestic 12',
            sip_key='majestic-12'
        )
        self.campaign = Campaign.objects.create(
            title='Test campaign',
            start_date='2000-01-01',
            end_date='2100-12-31',
            organization=self.organization)
        GroupType.objects.create(
            organization=self.organization,
            name='group'
        )
        GroupType.objects.create(
            organization=self.organization,
            name='chamber'
        )
        GroupType.objects.create(
            organization=self.organization,
            name='committee'
        )

    def test_str(self):
        importer = Importer.objects.create(
            name='test importer',
            category='repr',
            campaign=self.campaign,
        )
        self.assertEqual(str(importer), u'%(name)s: %(format)s %(type)s importer' % {'format': importer.category,
                                                'type': importer.kind(),
                                                'name': importer.name})

    def test_kind(self):
        importer = Importer.objects.create(
            name='test importer',
            category='repr',
            campaign=self.campaign,
        )
        self.assertEqual(importer.kind(), None)
        importer.file = 'a'
        importer.url = ''
        self.assertEqual(importer.kind(), 'file')
        importer.file = None
        importer.url = 'http://www.example.com/'
        self.assertEqual(importer.kind(), 'url')

    def test_representative_json(self):
        json_file = None
        with open('picampaign/importer/data_tests.json', u'rb') as f:
            json_file = SimpleUploadedFile('data_tests.json', f.read())

        importer = Importer.objects.create(
            name='test importer',
            category='repr',
            campaign=self.campaign,
            file=json_file,
            url=''
        )
        self.assertEqual(importer.kind(), 'file')
        importer.handle()
        importer.refresh_from_db()
        # The file import 3 of the 10 meps
        self.assertEqual(importer.last_count, 10)
        self.assertEqual(importer.last_imported, 3)
        self.assertEqual(len(Group.objects.all()), 13)
        self.assertEqual(len(Contact.objects.all()), 3)

    def test_representative_csv(self):
        csv_file = None
        with open('picampaign/importer/data_tests.csv', u'rb') as f:
            csv_file = SimpleUploadedFile('data_tests.csv', f.read())

        importer = Importer.objects.create(
                name='test updater',
                category='update',
                campaign=self.campaign,
                file=csv_file,
                url=''
        )
        self.assertEqual(importer.kind(), 'file')
        importer.handle()
        importer.refresh_from_db()
        # There's 2 Meps in the file
        self.assertEqual(importer.last_count, 2)
        self.assertEqual(importer.last_imported, 2)
        self.assertEqual(len(Group.objects.all()), 4)
        self.assertEqual(len(Contact.objects.all()), 2)

    def test_uniqueness_insert_10(self):
        '''
        This is related to isse #10
        '''
        # First a classic import.
        json_file = None
        with open('picampaign/importer/data_tests.json', u'rb') as f:
            json_file = SimpleUploadedFile('data_tests.json', f.read())

        importer = Importer.objects.create(
            name='test importer',
            category='repr',
            campaign = self.campaign,
            file=json_file,
            url=''
        )
        self.assertEqual(importer.kind(), 'file')
        importer.handle()
        importer.refresh_from_db()
        # The file import 3 of the 10 meps
        self.assertEqual(importer.last_count, 10)
        self.assertEqual(importer.last_imported, 3)
        self.assertEqual(len(Group.objects.all()), 13)
        self.assertEqual(len(Contact.objects.all()), 3)
        self.assertEqual(len(CampaignContact.objects.all()), 3)
        # If we rerun the same import we must have
        # different values
        importer.handle()
        importer.refresh_from_db()
        self.assertEqual(importer.last_count, 10)
        self.assertEqual(importer.last_imported, 3) # We still have 3 contact imported
        self.assertEqual(len(Group.objects.all()), 13) # Still the same groups
        self.assertEqual(len(Contact.objects.all()), 3) # No more contacts though
        self.assertEqual(len(CampaignContact.objects.all()), 3) # Or ContactCampaign
