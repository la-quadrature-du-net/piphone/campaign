# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0001_initial'),
        ('organization', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Feedback',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('comment', models.CharField(max_length=512, blank=True)),
                ('callee', models.ForeignKey(to='campaign.CampaignContact')),
                ('category', models.ForeignKey(to='organization.FeedbackCategory')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
