from django.contrib import admin
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponseRedirect

from picampaign.organization.models import FeedbackCategory
from picampaign.feedback.models import Feedback

class FeedbackFilterByCategory(admin.SimpleListFilter):
    title = 'category'
    parameter_name = 'category'

    def lookups(self, request, model_admin):
        if request.user.is_superuser:
            feedbacks = Feedback.objects.all().values_list(
                    'category__id',
                    'category__name')
        else:
            user_orgs = [x.id for x in request.user.organizations.all()]
            feedbacks = Feedback.objects.select_related('category').filter(
                category__organization__in=user_orgs).values_list(
                            'category__id',
                            'category__name')

        return ((f[0], f[1],) for f in sorted(set(feedbacks), key=lambda f: f[1]))

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(category__id=self.value())
        return queryset


class FeedbackFilterByOrganization(admin.SimpleListFilter):
    title = 'organization'
    parameter_name = 'organization'

    def lookups(self, request, model_admin):
        if request.user.is_superuser:
            feedbacks = Feedback.objects.all().values_list(
                    'category__organization__id',
                    'category__organization__name')
        else:
            user_orgs = [x.id for x in request.user.organizations.all()]
            feedbacks = Feedback.objects.filter(
                    category__organization__in=user_orgs).values_list(
                        'category__organization__id',
                        'category__organization__name')
        return ((f[0], f[1],) for f in sorted(set(feedbacks), key=lambda f: f[1]))

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(category__organization__id=self.value())
        return queryset


class FeedbackFilterByCampaign(admin.SimpleListFilter):
    title = 'campaign'
    parameter_name = 'campaign'

    def lookups(self, request, model_admin):
        if request.user.is_superuser:
            feedbacks = Feedback.objects.all().values_list(
                    'callee__campaign__id',
                    'callee__campaign__title')
        else:
            user_orgs = [x.id for x in request.user.organizations.all()]
            feedbacks = Feedback.objects.filter(
                    category__organization__in=user_orgs).values_list(
                        'callee__campaign__id',
                        'callee__campaign__title')
        return ((f[0], f[1],) for f in sorted(set(feedbacks), key=lambda f: f[1]))

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(callee__campaign__id=self.value())
        return queryset


class FeedbackAdmin(admin.ModelAdmin):
    date_hierarchy = 'date'
    list_display = ('callee', 'category',
            'date', 'organization',)
    list_filter = (FeedbackFilterByCategory,
            FeedbackFilterByOrganization,
            FeedbackFilterByCampaign)
    search_fields = ['category__organization__name', 'category__name',
            'callee__contact__first_name',
            'callee__contact__last_name',
            'callee__campaign__title']
    actions = ['export_csv']

    def get_queryset(self, request):
        qs = super(FeedbackAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        user_orgs = [x.id for x in request.user.organizations.all()]
        return qs.filter(category__organization__in=user_orgs)

    def export_csv(self, request, queryset):
        selected = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        ct = ContentType.objects.get_for_model(queryset.model)
        return HttpResponseRedirect("/export/?ct={}&ids={}".format(ct.pk, ",".join(selected)))

    export_csv.short_description = "Export feedbacks as CSV"

admin.site.register(Feedback, FeedbackAdmin)
