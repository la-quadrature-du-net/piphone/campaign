import json
import csv

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.contenttypes.models import ContentType
from rest_framework import viewsets
from rest_framework.response import Response

from picampaign.feedback.serializers import FeedbackSerializer
from picampaign.feedback.models import Feedback, FeedbackCategory
from picampaign.contact.models import Contact


class FeedbackViewSet(viewsets.ViewSet):

    queryset = Feedback.objects.all()
    serializer_class = FeedbackSerializer

    @csrf_exempt
    def create(self, request, campaign_pk=None):
        serializer = FeedbackSerializer(data=request.data)
        try:
            serializer.is_valid(raise_exception=True)
            feedback = serializer.create(serializer.validated_data)
            return Response(feedback.id)
        except Exception as e:
            raise e


def FeedbackExportCSVView(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="feedbacks.csv"'

    model = ContentType.objects.get_for_id(int(request.GET['ct']))
    feedbacks = model.model_class().objects.filter(
            pk__in=[int(pk) for pk in request.GET['ids'].split(',')]).select_related(
                    'callee', 'callee__contact', 'callee__campaign', 'category')

    writer = csv.writer(response)
    # A Header is nice
    writer.writerow(['Date', 'Contact', 'Campaign', 'Category', 'Comment'])
    for feedback in feedbacks:
        writer.writerow([feedback.date,
            feedback.callee.contact.full_name,
            feedback.callee.campaign,
            feedback.category,
            feedback.comment])

    return response
