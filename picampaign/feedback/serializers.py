from picampaign.feedback.models import Feedback
from rest_framework import serializers


class FeedbackSerializer(serializers.ModelSerializer):

    class Meta:
        model = Feedback
        fields = ('category', 'comment', 'callee')
