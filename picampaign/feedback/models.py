from django.db import models
from picampaign.campaign.models import CampaignContact
from picampaign.organization.models import FeedbackCategory
from django.utils.translation import ugettext_lazy as _


class Feedback(models.Model):
    id = models.AutoField(primary_key=True)
    callee = models.ForeignKey(CampaignContact)
    category = models.ForeignKey(FeedbackCategory)
    comment = models.CharField(max_length=512, blank=True)
    date = models.DateTimeField(auto_now_add=True)

    def organization(self):
        return self.category.organization

    def __str__(self):
        return _('feedback for %(callee contact)s on %(campaign title)s') % \
            {'callee contact': self.callee.contact,
             'campaign title': self.callee.campaign.title}
