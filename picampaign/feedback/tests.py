from django.test import TestCase
from rest_framework.test import APITestCase

from picampaign.contact.models import Contact
from picampaign.campaign.models import CampaignContact, Campaign
from picampaign.organization.models import FeedbackCategory, Organization
from picampaign.feedback.models import Feedback

class FeedbackModelTest(TestCase):
    def test_str(self):
        organization = Organization.objects.create(
            name='Majestic 12',
            sip_key='majestic-12'
        )
        contact = Contact.objects.create(
            first_name='Victor',
            last_name='Hugo',
            birthdate='1889-02-26'
        )
        campaign = Campaign.objects.create(
            title='Test campaign',
            start_date='2000-01-01',
            end_date='2100-12-31',
            organization=organization
        )
        campaigncontact = CampaignContact.objects.create(
            campaign=campaign,
            contact=contact
        )
        feedbackcategory = FeedbackCategory.objects.create(
            name='Normal',
            organization=organization
        )
        feedback = Feedback.objects.create(
            callee=campaigncontact,
            category=feedbackcategory,
            comment='Feedback'
        )
        self.assertEqual(str(feedback), 'feedback for %(callee)s on %(campaign)s' %
                {'callee': campaigncontact.contact,
                    'campaign': campaign})

class FeedbackViewTest(APITestCase):
    def test_create(self):
        organization = Organization.objects.create(
            name='Majestic 12',
            sip_key='majestic-12'
        )
        category = FeedbackCategory.objects.create(
            name='Normal',
            organization=organization
        )
        campaign = Campaign.objects.create(
            title='Test campaign',
            start_date='2000-01-01',
            end_date='2100-12-31',
            organization=organization
        )
        contact = Contact.objects.create(
            first_name='Victor',
            last_name='Hugo',
            birthdate='1889-02-26'
        )
        campaigncontact = CampaignContact.objects.create(
            campaign=campaign,
            contact=contact
        )
        data = {'callee': contact.id, 'category': category.id, 'comment': 'Feedback'}
        response = self.client.post('/campaigns/%(cid)d/feedbacks/' % {'cid': campaign.id}, data, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Feedback.objects.count(), 1)
        self.assertEqual(Feedback.objects.get().comment, 'Feedback')
        # A category which does not exists should raise an error
        data = {'callee': contact.id, 'category': 1234567, 'comment': 'Feedback'}
        response = self.client.post('/campaigns/%(cid)d/feedbacks/' % {'cid': campaign.id}, data, format='json')
        assert response.status_code >= 400
