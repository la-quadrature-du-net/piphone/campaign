import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.md')).read()

requires = [
    'django',
    'djangorestframework',
    'drf-nested-routers',
    'drfdocs',
    'django-modeltranslation',
    'requests',
    'pillow',
    'django-ckeditor',
    'django-wysiwyg',
    'django-cors-headers',
]

setup(name='picampaign',
      namespace_packages=['picampaign'],
      version='0.1.1',
      description='PiPhone Campaign Manager.',
      long_description=README,
      classifiers=["Programming Language :: Python"],
      author='',
      author_email='',
      url='',
      keywords='web django application',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      tests_require=requires,
      )
